import React from "react";
import ReactDom from "react-dom";
import {Pane, Table} from "react-photonkit";
import MonitorMachine from './monitorComponent/monitor.machine.poll.jsx';
import MonitorTableRow from './monitorComponent/monitor.table.row.jsx';

require('../styles/overview.scss');
require('react-dom');

var thisClass;
var connectArray = [];
var express = window.require('electron').remote.require('express');
var path = window.require('electron').remote.require('path');
var favicon = window.require('electron').remote.require('serve-favicon');
var express_logger = window.require('electron').remote.require('morgan');

var logStateEnum = {
    INFO: 1,
    WARNING: 2,
    ERROR: 3,
    DEBUG: 4
};
var app = express();
var lastTimeData;
//View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express_logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
//
app.use(function (req, res, next) {
    req.json = {
        "title": config.title,
        "animation": config.animation,
        "header": config.header ? config.header : config.title,
        "miners": miners.json,
        "refresh": config.web_refresh,
        "tolerance": config.tolerance,
        "temperature": config.temperature,
        "hashrates": config.hashrates,
        "updated": moment().format("YYYY-MM-DD HH:mm:ss")
    };
    next();
});
var machine = [];
var haveNoMachine = false;
function removeSpace(str) {
    return str.replace(/\s+/g, '');
}


function getConfigData() {
    var jsonfile = window.require('electron').remote.require('jsonfile');
    var file = './config.json';
    var data = jsonfile.readFileSync(file);
    return data;
}
var state = 1;

//Catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// Error handlers

// Development error handler will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// Production error handler, no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//module.exports = app;

//
var config = window.require('electron').remote.require('./config.json');

var log4js = window.require('electron').remote.require('log4js');
var logger = log4js.getLogger();
logger.setLevel(config.log_level ? config.log_level : 'INFO');

logger.warn('app: booting');

// --------------- /BOOT ---------------

// --------------- REQUESTER ---------------

var io = window.require('electron').remote.require('socket.io-client');

var net = window.require('electron').remote.require('net');
var moment = window.require('electron').remote.require('moment');
window.require('electron').remote.require("moment-duration-format");


var miners = [];
miners.json = [];


class Monitor extends React.Component {


    constructor(props) {
        super(props);
        thisClass = this;
        this.state = {
            isActive: this.props.isActive,
            object: null,
            machine: [],
            number: 0,
            log: []
        };
        this.readTextFile(0);


    }


    addExistMachine(object) {
        var key = Math.random();

        this.state.machine.push(<MonitorMachine  id={this.state.machine.length}
                                                key={key} log={this.state.log} object={object}></MonitorMachine>);
        this.reload();
    }

    reload() {
        this.setState({object: ""});
    }


    readTextFile(position) {
        var jsonfile = window.require('electron').remote.require('jsonfile');
        var file = './config.json';
        var data = jsonfile.readFileSync(file);
        if (data != "") {
            var count = data.miners.length;
            if (count !== 'undefined' || count > 0) {
                for (var index = position; index < count; index++) {
                    this.addExistMachine(data.miners[index]);
                }
            }
        }
    }

    removeAll() {

    }

    stopEveryThing() {
        haveNoMachine = true;
        this.state.machine = [];
        for (var index = 0; index < connectArray.length; index++) {
            connectArray[index].destroy();
        }
        this.setState({});
    }

    componentWillReceiveProps(nextProps) {

        this.updateMachine();
    }

    updateMachine(){
        var tempData = getConfigData();

        if (tempData.miners.length == 0) {
            this.stopEveryThing();
            this.reload();
            return;
        }
        if (tempData.miners.length <= this.state.machine.length) {
            this.checkAndRemove(tempData);
        } else {
            this.checkAndAdd(tempData);
        }
    }

    checkAndAdd(tempData){
        for (var index = 0; index < tempData.miners.length; index++) {
            if (index == this.state.machine.length || index > this.state.machine.length) {
                this.readTextFile(index);
                this.startListenner(index);
                this.forceUpdate();

            } else {
                var currentObj = this.state.machine[index];
                var latestObj = tempData.miners[index];
                if (latestObj.name != currentObj.props.object.name) {
                    currentObj.props.object.name = latestObj.name;
                    miners[index].name = latestObj.name;
                }

                if (currentObj.props.object.host != latestObj.host || currentObj.props.object.port != latestObj.port) {
                    miners[index].host = latestObj.host;
                    miners[index].port = latestObj.port;
                    currentObj.props.object.host = latestObj.host;
                    currentObj.props.object.port = latestObj.port;
                    connectArray[index].destroy();
                    connectArray[index].connect(latestObj.port, latestObj.host);
                }
                this.forceUpdate();
            }
        }
        this.forceUpdate();

    }

    checkAndRemove(tempData){
        for (var index = 0; index < this.state.machine.length; index++) {
            if (index == tempData.miners.length || index > tempData.miners.length) {
                connectArray[index].destroy();
                this.state.machine.splice(index, 1);
                this.forceUpdate();

            } else {
                var currentObj = this.state.machine[index];
                var latestObj = tempData.miners[index];
                if (latestObj.name != currentObj.props.object.name) {
                    currentObj.props.object.name = latestObj.name;
                    miners[index].name = atestObj.name;

                }
                if (currentObj.props.object.host != latestObj.host || currentObj.props.object.port != latestObj.port) {
                    miners[index].host = latestObj.host;
                    miners[index].port = latestObj.port;
                    currentObj.props.object.host = latestObj.host;
                    currentObj.props.object.port = latestObj.port;
                    connectArray[index].destroy();
                    connectArray[index].connect(latestObj.port, latestObj.host);
                }
                this.forceUpdate();

            }
        }
        this.forceUpdate();

    }


    addLogMachine(machineId, logState, content) {
        var stateString = "";
        var stateClass = "";
        if (logState == logStateEnum.INFO) {
            logger.info(content);
            stateString = "Thành công";
            stateClass = "info";
        } else if (logState == logStateEnum.WARNING) {
            logger.warn(content);
            stateString = "Cảnh báo";
            stateClass = "warn";
        } else if (logState == logStateEnum.DEBUG) {
            logger.trace(content);
            stateString = "Gỡ rối";
            stateClass = "debug";
        } else if (logState == logStateEnum.ERROR) {
            logger.error(content);
            stateString = "Thất bại";
            stateClass = "error";
        } else {
            return;
        }

        var fullDate = moment().format("YYYY-MM-DD HH:mm:ss");
        var elements = document.getElementsByClassName(machineId);
        var offsetHeight = 300;
        //if(elements[0].offsetHeight !== undefined){
        //    offsetHeight = elements[0].offsetHeight;
        //}
        //var scrollTop = elements[0].scrollTop;
        //var scrollHeight = elements[0].scrollHeight;
        //var enalbeScroll = false;
        //
        //if((offsetHeight + scrollTop) === (scrollHeight-2) || (offsetHeight + scrollTop) === (scrollHeight+2)){
        //    enalbeScroll = true;
        //}
        //
        var temp = this.state.machine;
        this.state.machine = [];
        this.reload();
        for (var index = 0; index < temp.length; index++) {
            var machineObj = temp[index];
            var key = Math.random();
            if (machineObj.props.object.name == machineId) {

                machineObj.props.log.push(<MonitorTableRow
                    key={key}
                    log={{
                        activity: content,
                        state: stateString,
                        time: fullDate,
                        stateClass: stateClass
                    }}>
                </MonitorTableRow>);
                break;
            }
        }
        this.state.machine = temp;
        this.reload();
    }


    startListenner(index) {
        config = getConfigData();
        config.miners.forEach(function (item, i, arr) {
            if (i == index || i > index) {
                logger.trace(item.name + ': config[' + i + ']');

                // settings
                var m = miners[i] = {};
                var c = config.miners[i];
                var j = miners.json[i];
                var s = config.server_monitor;

                m.name = c.name;
                m.email = config.email;
                m.host = c.host;
                m.port = c.port;

                m.poll = (typeof c.poll !== 'undefined') ? c.poll : config.miner_poll;
                m.timeout = (typeof c.timeout !== 'undefined') ? c.timeout : config.miner_timeout;

                var a = {};
                a.app_name = config.name;
                a.app_version = config.version;
                a.email = m.email;
                a.name = m.name;


                function hostname() {
                    return c.hostname ? c.hostname : (m.host + ':' + m.port);
                }

                // stats
                m.reqCnt = 0;
                m.rspCnt = 0;

                // it was never seen and never found good yet
                c.last_seen = null;
                c.last_good = null;


                // Keep connect to management server
                m.serverSocket = io.connect('http://' + s.host + ':' + s.port, {reconnect: true});

                // Add a connect listener
                m.serverSocket.on('connect', function (socket) {
                    thisClass.addLogMachine(m.name, logStateEnum.INFO, 'Kết nối đến máy chủ đám mây.');

                    m.serverSocket.emit('app-info', m.name, a); // Submit app info
                    thisClass.addLogMachine(m.name, logStateEnum.INFO, 'Gửi thông tin giàn đào lên máy chủ đám mây.');

                });


                // request Claymores for get miner information
                m.socket = new net.Socket()

                    .on('connect', function () {
                        var req = '{"id":0,"jsonrpc":"2.0","method":"miner_getstat1"}';
                        ++m.reqCnt;
                        logger.trace(m.name + ': req[' + m.reqCnt + ']: ' + req);

                        m.socket.write(req + '\n');
                        m.socket.setTimeout(m.timeout);
                    })

                    .on('timeout', function (e) {
                        thisClass.addLogMachine(m.name, logStateEnum.WARNING, 'Phần mềm Claymores bị đứng.');

                        m.socket.destroy();

                        miners.json[i] = {
                            "name": m.name,
                            "email": m.email,
                            "host": hostname(),
                            "uptime": "",
                            "eth": "",
                            "dcr": "",
                            "eth_hr": "",
                            "dcr_hr": "",
                            "temps": "",
                            "pools": "",
                            "ver": "",
                            "target_eth": "",
                            "target_dcr": "",
                            "comments": c.comments,
                            "offline": c.offline,
                            "warning": "Phần mềm Claymores bị đứng",
                            "error": "",
                            "last_seen": c.last_seen ? c.last_seen : 'never'
                        };
                    })

                    .on('data', function (data) {
                        ++m.rspCnt;
                        logger.trace(m.name + ': rsp[' + m.rspCnt + ']: ' + data.toString().trim());
                        c.last_seen = moment().format("YYYY-MM-DD HH:mm:ss");
                        m.socket.setTimeout(0);
                        var d = JSON.parse(data);
                        miners.json[i] = {
                            "name": m.name,
                            "email": m.email,
                            "host": hostname(),
                            "uptime": moment.duration(parseInt(d.result[1]), 'minutes').format('d [ngày,] hh [giờ] mm [phút]'),
                            "eth": d.result[2],
                            "dcr": d.result[4],
                            "eth_hr": d.result[3],
                            "dcr_hr": d.result[5],
                            "temps": d.result[6],
                            "pools": d.result[7],
                            "ver": d.result[0],
                            "target_eth": c.target_eth,
                            "target_dcr": c.target_dcr,
                            "comments": c.comments,
                            "offline": c.offline,
                            "ti": c.ti ? c.ti : null,
                            "error": null
                        };
                        if (c.target_eth && config.tolerance) {
                            if (miners.json[i].eth.split(';')[0] / 1000 < c.target_eth * (1 - config.tolerance / 100)) {
                                miners.json[i].warning = 'Tốc độ đào đang chậm';
                                miners.json[i].last_good = c.last_good ? c.last_good : 'never';
                            } else {
                                miners.json[i].warning = null;
                                c.last_good = moment().format("YYYY-MM-DD HH:mm:ss");
                            }
                        }

                        var totalHash = 0;
                        var hashInfo = miners.json[i].eth.split(";");
                        if (hashInfo.length > 0) {
                            totalHash = hashInfo[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                        }

                        var content = "Tổng tốc độ đào hiện tại là: " + totalHash + " Hash/s";
                        thisClass.addLogMachine(m.name, logStateEnum.INFO, content);

                        if (m.serverSocket != null && m.serverSocket.connected) {
                            m.serverSocket.emit('miner', "CLAYMORES", miners.json[i]); // Submit miner info
                            thisClass.addLogMachine(m.name, logStateEnum.INFO, "Gửi thông tin dàn đào lên máy chủ đám mây");
                        }
                    })

                    .on('close', function () {
                        if (haveNoMachine == false) {
                            setTimeout(poll, m.poll);
                        }
                    })

                    .on('error', function (e) {
                        thisClass.addLogMachine(m.name, logStateEnum.WARNING, 'Không thể kết nối được giàn đào. Do địa chỉ IP sai hoặc phần mềm đào đã tắt.');
                        miners.json[i] = {
                            "name": m.name,
                            "email": m.email,
                            "host": hostname(),
                            "uptime": "",
                            "eth": "",
                            "dcr": "",
                            "eth_hr": "",
                            "dcr_hr": "",
                            "temps": "",
                            "pools": "",
                            "ver": "",
                            "target_eth": "",
                            "target_dcr": "",
                            "comments": c.comments,
                            "offline": c.offline,
                            "warning": "Địa chỉ IP không phù hợp để lấy thông tin từ Claymore",
                            "error": e.name + ': ' + e.message,
                            "last_seen": c.last_seen ? c.last_seen : 'never'
                        };
                    });

                function poll() {
                    m.socket.connect(m.port, m.host);
                }

                if ((typeof c.offline === 'undefined') || !c.offline) {
                    poll();
                } else {
                    miners.json[i] = {
                        "name": m.name,
                        "host": hostname(),
                        "uptime": "",
                        "eth": "",
                        "dcr": "",
                        "eth_hr": "",
                        "dcr_hr": "",
                        "temps": "",
                        "pools": "",
                        "ver": "",
                        "target_eth": "",
                        "target_dcr": "",
                        "comments": c.comments,
                        "offline": c.offline,
                        "error": null
                    };
                }
                connectArray.push(m.socket);
            }
        });
    }

    componentDidMount() {
        this.startListenner(0);
    }

    render() {
        return (
            <Pane className={this.props.isActive ? 'padded-more ' : 'padded-more hidden'}>
                {this.state.machine.map(function (input, index) {
                    return input;
                })}
            </Pane>
        );
    }
}

export default Monitor
