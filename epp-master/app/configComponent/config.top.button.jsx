import React from "react";
import ReactDom from "react-dom";
import { Pane, Button, ButtonGroup } from "react-photonkit";
import Config from '../config.container.jsx';

require('react-dom');
require('../../styles/setting.scss');

class ConfigTitle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            //<PaneGroup>
            //<Pane>
                <ButtonGroup>
                    <Button text="Xoá thay đổi" glyph="reply" ptStyle="negative" onClick={() => this.props.onClick("reset")}></Button>
                    <Button text="Thêm máy mới" glyph="plus" ptStyle="primary" onClick={() => this.props.onClick("add")}></Button>
                    <Button text="Lưu thay đổi" glyph="floppy" ptStyle="positive" onClick={() => this.props.onClick("save")}></Button>
                </ButtonGroup>
                //<div className="float_right" id="update_success">Cập nhật thành công</div>
            //</Pane>
            //</PaneGroup>
        );

    }
}

export default ConfigTitle
