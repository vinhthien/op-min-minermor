import React from "react";
import ReactDom from "react-dom";
import { Pane, Input, Button } from "react-photonkit";
import ConfigIpInput from './config.ip.input.jsx';

require('react-dom')
require('../../styles/setting.scss');


class ConfigMachine extends React.Component {
    constructor(props) {
        super(props);
        this.circumstances = {
            foundedSpecialCharacter: 1,
            unExpectedLengthString: 2,
            permittedLengthString: 3,
            foundedAlphaBet: 4
        };
        this.input = {
            input1: 1,
            input2: 2,
            input3: 3,
            input4: 4,
            port: 5
        }
        this.notification = null;
    }


    updateInputValue(evt){
        var name = evt.target.value;
        var label = this.refs.name;
        label.innerHTML = "cau hinh " + name;
        this.props.machineName.name = name;
    }
    updatePoll(evt){
        var poll = evt.target.value;
        this.props.machineName.poll = poll;
    }
    updateHost(evt){
        var host = this.refs.host;
        var ip1 = host.props.machineName.ip1;
        var ip2 = host.props.machineName.ip2;
        var ip3 = host.props.machineName.ip3;
        var ip4 = host.props.machineName.ip4;
        var port = host.props.machineName.port;

        this.props.machineName.ip1 = ip1;
        this.props.machineName.ip2 = ip2;
        this.props.machineName.ip3 = ip3;
        this.props.machineName.ip4 = ip4;
        this.props.machineName.port = port;

    }
    render() {
        return (
            <div className="main_content margin_top_twenty">
                <div className="float_left margin_left_twenty main_content_title" ref="name">Cau
                    hinh {this.props.machineName.name}</div>
                <div className="delete" id={this.props.machineId}
                     onClick={evt => {this.props.onClick(evt)}}>X</div>
                <div className="main_content_element">
                    <div className="float_left full_width ">Ten</div>
                    <br/>
                    <input className="main_content_input_large "
                           onChange={evt => {this.updateInputValue(evt);this.props.onClick()}}
                           id="name" defaultValue={this.props.machineName.name}
                           placeholder="May 1"/>

                    <div className="float_left full_width margin_top">Pools</div>
                    <input className="main_content_input_large margin_top" id="pool"
                           defaultValue={this.props.machineName.poll}
                           onChange={evt => {this.updatePoll(evt);this.props.onClick()}}
                           placeholder="Nhap dia chi Pool"/>

                    <div className="float_left full_width margin_top">Dia chi may (IP)</div>
                    <ConfigIpInput machineName={this.props.machineName} ref="host" onClick={evt => {this.updateHost(evt);this.props.onClick()}}></ConfigIpInput>
                    <br/> <br/><br/></div>
            </div>
        );
    }
}

export default ConfigMachine
