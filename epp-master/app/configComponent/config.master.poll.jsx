import React from "react";
import ReactDom from "react-dom";
import { Pane, Input, Button } from "react-photonkit";

require('react-dom')
require('../../styles/setting.scss');

class ConfigMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            enable: true
        }
    }
    componentDidMount() {
        this.state.enable = this.props.enable;
    }
    onchange(){
        this.refs.onClick();
    }
    render() {
        if(this.props.enalbe){
            return (
                <div className="main_content master_pool">
                    <div className="float_left margin_left_twenty main_content_title">Cau hinh pool</div>
                    <div className="main_content_element">
                        <div className="float_left margin_top">Bat chuc nang cau hinh Pool ap dung
                            cho yay ca cac may <input onChange={this.onchange.bind(this)} type="checkbox" name="vehicle" id="checkbox" checked="{this.props.enable}"  />
                        </div>
                        <Input className="main_content_input_large" value={this.props.poll}   id="main_pool"
                               placeholder="Nhập địa chỉ Pool..."/>
                    </div>
                </div>
            );
        }else{
            <div className="main_content master_pool">
                <div className="float_left margin_left_twenty main_content_title">Cau hinh pool</div>
                <div className="main_content_element">
                    <div className="float_left margin_top">Bat chuc nang cau hinh Pool ap dung
                        cho yay ca cac may <input onClick={this.switch} type="checkbox" name="vehicle" id="checkbox"  />
                    </div>
                    <Input className="main_content_input_large" value={this.props.poll}   id="main_pool"
                           placeholder="Nhập địa chỉ Pool..."/>
                </div>
            </div>
        }

    }
}

export default ConfigMain
