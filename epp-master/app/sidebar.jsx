import React from "react";
import { Pane, NavGroup, NavTitle, NavGroupItem } from "react-photonkit";


class Sidebar extends React.Component {
    //onSelect(index) {
    //  //this.props.handleSelect();
    //  console.log(`sidebar clicked with ${index}`)
    //}

    updateState() {
        if(this.props.change == 1){
            this.showOverview();
        }else{
            this.showConfig();
        }
    }
    showConfig(){
        setTimeout(function () {
            var config = document.getElementById("config");
            config.classList.add("active");
            var overview = document.getElementById("overview");
            overview.classList.remove("active");
        }, 50);

    }
    showOverview(){
        setTimeout(function () {  var overview = document.getElementById("overview");
                overview.classList.add("active");
                var config = document.getElementById("config");
                config.classList.remove("active");
        }, 50);
    }
    render() {
        return (
            <Pane ptSize="sm" sidebar>
                <NavGroup ref="main" activeKey={1} onSelect={this.props.handleSelect}>
                    <NavTitle>Hoạt động</NavTitle>
                    <NavGroupItem id="overview" ref="overview" onClick={this.updateState()} eventKey={1} glyph="monitor"
                                  text="Dàn đào"/>
                    <NavGroupItem id="config" ref="config" onClick={this.updateState()} eventKey={2} glyph="tools"
                                  text="Thiết lập"/>
                </NavGroup>
            </Pane>
        );
    }
}

export default Sidebar;
