import React from "react";
import ReactDom from "react-dom";
import {Pane, Input, Button} from "react-photonkit";

require('react-dom')
require('../../styles/overview.scss');
function removeSpace(str) {
    return str.replace(/\s+/g, '');
}

class MonitorTableRow extends React.Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <tr  >
                <th className="wrap text_one">{this.props.log.activity}</th>
                <th className={"wrap twenty_percent " + this.props.log.stateClass}  >
                    {this.props.log.state}</th>
                <th className="wrap twenty_percent">{this.props.log.time}</th>
            </tr>
        );
    }
}

export default MonitorTableRow
