import React from "react";
import ReactDom from "react-dom";
import {Pane, Input, Button} from "react-photonkit";
import MonitorTableRow from './monitor.table.row.jsx';

require('react-dom')
require('../../styles/overview.scss');
function removeSpace(str) {
    return str.replace(/\s+/g, '');
}

class MonitorMachine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            log: this.props.log
        }
        // this.addLog();
    }

    // addLog(){
    //     for(var index = 0; index < this.props.log.length; index ++){
    //         this.state.log.push(this.props.log[index]);
    //     }
    // }
    componentDidMount() {
        var main = this.refs.main;
        this.refs.main.scrollTop = this.refs.main.scrollHeight;

    }

    componentWillReceiveProps(nextProps) {
            var i = 0;
            var a = 0;
            var b = 0;
    }

    componentDidUpdate(prevProps, prevState) {
        // only update chart if the data has changed
        if (prevProps.data !== this.props.data) {

        }
    }
    shouldComponentUpdate(){
        var i = 0;
        var a = 0;
        var b = 0;
    }
    componentWillUpdate(){
        var i = 0;
        var a = 0;
        var b = 0;
    }
    componentDidUpdate(){
        var i = 0;
        var a = 0;
        var b = 0;
    }

    render() {
        return (
            <div  id={removeSpace(this.props.object.name)} className="main_element">
                <p className="machine-title">{this.props.object.name}</p>
                <div className="four">
                    <div className="one">Hoạt động</div>
                    <div className="two">Tình trạng</div>
                    <div className="three">Thời điểm</div>
                </div>
                <table ref="main" className="table-striped ">

                    <tbody >
                    {this.props.log}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default MonitorMachine
