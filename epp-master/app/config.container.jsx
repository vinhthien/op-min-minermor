import React from "react";
import ReactDom from "react-dom";
import {Pane, Input, Button} from "react-photonkit";
import ConfigTitle from './configComponent/config.top.button.jsx';
import ConfigMain from './configComponent/config.master.poll.jsx';
import ConfigMachine from './configComponent/config.machine.poll.jsx';

require('react-dom');
require('../styles/setting.scss');
var data = "";
var miner = [];
function readTextFile() {
    const remote = window.require('electron').remote;
    var jsonfile = remote.require('jsonfile');
    var file = './config.json';
    data = jsonfile.readFileSync(file);
    miner = [];
    for (var index = 0; index < data.miners.length; index++) {
        miner.push(data.miners[index]);
    }
    //var data = jsonfile.readFile(file, function (err, obj) {
    //    data1 = obj;
    //
    //});

}
function resetMiner() {
    const remote = window.require('electron').remote;
    var jsonfile = remote.require('jsonfile');
    var file = './config.json';
    var data = jsonfile.readFileSync(file);
    miner = [];
    for (var index = 0; index < data.miners.length; index++) {
        miner.push(data.miners[index]);
    }
}
function getMachineList() {
    const remote = window.require('electron').remote;
    var jsonfile = remote.require('jsonfile');
    var file = './config.json';
    data = jsonfile.readFileSync(file);
    return data.miners
}
var hasChange = false;

readTextFile();
class Config extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: this.props.isActive,
            machine: [],
            ip: [],
            enableMasterPoll: true,
            hasChange: false
        };
        this.addMasterPoll();
    }


    componentDidMount() {
        //this.addMasterPoll();
        this.useData();
    }

    changeMasterPollState() {
        this.state.enableMasterPoll = false;
    }

    addMasterPoll() {
        var pollContent = "";
        if (data.force_pool_enable == false) {
            this.state.enableMasterPoll = "false";
        } else {
            pollContent = data.force_pool;
        }
        var refs = "machine" + this.state.machine.length;
        this.state.machine.push(<ConfigMain text={this.state.text}
                                            onClick={() => this.changeMasterPollState()} key={this.state.machine.length}
                                            enalbe={this.state.enableMasterPoll}
                                            pool={pollContent}></ConfigMain>);
    }

    useData() {
        var count = data.miners.length;
        if (count !== 'undefined' || count > 0) {
            for (var index = 0; index < count; index++) {
                this.addExistMachine(data.miners[index]);
            }
        }

    }

    updateData() {
        var tempArray = [];
        var count = this.state.machine.length;
        for (var index = 1; index < count; index++) {
            var machine = this.state.machine[index].props.machineName;
            var ip1 = machine.ip1;
            var ip2 = machine.ip2;
            var ip3 = machine.ip3;
            var ip4 = machine.ip4;
            var name = machine.name;
            var port = machine.port;
            var poll = machine.poll;
            var ip = ip1 + "." + ip2 + "." + ip3 + "." + ip4;

            if (poll === undefined) {
                poll = "";
            }
            tempArray.push({
                "name": name,
                "host": ip,
                "port": port,
                "target_eth": 118,
                "target_dcr": 2700,
                "comments": "",
                "offline": false,
                "pool": pool
            });
        }
        data.miners = tempArray;
    }

    updateInfo() {

        var count = this.state.machine.length;
        var tempArray = [];
        this.state.ip = null;
        for (var index = 1; index < count; index++) {
            var position = index - 1;
            var machine = this.state.machine[index].props.machineName;
            var ip1 = machine.ip1;
            var ip2 = machine.ip2;
            var ip3 = machine.ip3;
            var ip4 = machine.ip4;
            var name = machine.name;

            var port = machine.port;
            var pool = machine.poll;
            if (pool === undefined || pool === null) {
                pool = "";
            }
            if (ip1 == "" || ip2 == "" || ip3 == "" || ip4 == "" || port == "" || pool == "") {
                alert("Điền đầy đủ thông tin trước khi lưu");
                return;
            }
            var host = ip1 + "." + ip2 + "." + ip3 + "." + ip4;
            tempArray.push({
                "name": name,
                "host": host,
                "port": port,
                "target_eth": 118,
                "target_dcr": 2700,
                "comments": "",
                "offline": false,
                "pool": pool
            });
        }
        data.miners = [];
        data.miners = tempArray;
        const remote = window.require('electron').remote;
        var jsonfile = remote.require('jsonfile');
        var file = './config.json';

        jsonfile.writeFileSync(file, data);
        this.state.hasChange = false;
        alert("Thay đổi thành công");

        //child_process.exec('ls', function (err, stdout, stderr){
        //    if (err) {
        //        console.log("child processes failed with error code: " +
        //            err.code);
        //    }
        //    console.log(stdout);
        //});

        var exec = window.require('electron').remote.require('child_process').exec;
        var isWin = /^win/.test(process.platform);
        var isLinux = /^linux/.test(process.platform);
        if (isWin) {
            exec('/Applications/TextEdit.app/Contents/MacOS/textedit', function callback(error, stdout, stderr){
                console('asds');
            });
        } else if (isLinux) {
            exec('/Applications/TextEdit.app/Contents/MacOS/textedit', function callback(error, stdout, stderr){
                console('asds');
            });
        }

    }

    checkForChange(str) {
        if (str === undefined) {
            if ((miner.length + 1) < this.state.machine.length) {
                this.state.hasChange = true;
                return false;
            }
            for (var index = 0; index < miner.length; index++) {
                var obj = miner[index];
                var machineObj = this.state.machine[index + 1];
                var ip1 = machineObj.props.machineName.ip1;
                var ip2 = machineObj.props.machineName.ip2;
                var ip3 = machineObj.props.machineName.ip3;
                var ip4 = machineObj.props.machineName.ip4;
                var port = machineObj.props.machineName.port;
                var poll = machineObj.props.machineName.poll;
                var name = machineObj.props.machineName.name;
                var ip = ip1 + "." + ip2 + "." + ip3 + "." + ip4;
                if (obj.host != ip || obj.name != name || obj.pool != poll || obj.port != port) {
                    //this.props.hasChange = true;
                    this.state.hasChange = true;
                    return false;
                } else {
                    //this.props.hasChange = false;
                    this.state.hasChange = false;
                }
            }
        } else {
            var id = parseInt(str.target.id) + 1;
            for (var index = 1; index < this.state.machine.length; index++) {
                var obj = this.state.machine[index];

                if (obj.props.machineId == str.target.id) {
                    this.state.machine.splice(index, 1);
                    break;
                }
            }
            this.setState({change: ""});
            this.state.hasChange = true;
        }

    }

    addExistMachine(object) {
        var ip = object.host;
        ip = ip.split(/[.\-_]/);
        var ip1 = ip[0];
        var ip2 = ip[1];
        var ip3 = ip[2];
        var ip4 = ip[3];
        if (ip1 === undefined || ip1 === "undefined") {
            ip1 = "";
        }
        if (ip2 === undefined || ip2 === "undefined") {
            ip2 = "";
        }
        if (ip3 === undefined || ip3 === "undefined") {
            ip3 = "";
        }
        if (ip4 === undefined || ip4 === "undefined") {
            ip4 = "";
        }
        var port = object.port;
        if (port === undefined || port === "undefined") {
            port = "";
        }
        var poll = object.pool;
        if (poll === undefined || poll === "undefined") {
            poll = "";
        }
        var name = object.name;
        if (name === undefined || name === "undefined") {
            name = "";
        }
        var object = {
            name: name,
            poll: poll,
            ip1: ip1,
            ip2: ip2,
            ip3: ip3,
            ip4: ip4,
            port: port
        }
        //const inputList = this.state.machine;
        //this.setState({
        //    machine: inputList.concat(<ConfigMachine key={this.state.machine.length} machineName={object}></ConfigMachine>)
        //});
        var key = Math.random();

        this.state.machine.push(<ConfigMachine key={key}
                                               machineId={(this.state.machine.length - 1)}
                                               onClick={(str) => {
                                                   this.checkForChange(str)
                                               }}
                                               machineName={object}></ConfigMachine>);
    }

    addEmptyMachine(event) {
        //document.getElementById("update_success").style.visibility = "invisible";
        var name = 'Máy ' + (parseInt(miner.length) + 2);
        var object = {
            name: name,
            ip1: "",
            ip2: "",
            ip3: "",
            ip4: "",
            port: ""
        }
        this.state.machine.push(<ConfigMachine
            machineId={this.state.machine.length}
            onClick={(str) => {
                this.checkForChange(str)
            }}
            key={this.state.machine.length} machineName={object}></ConfigMachine>);

        this.addDataToArray();
        this.setState({});

    }

    handleEvent(str) {
        if (str === "save") {
            this.updateInfo();
        } else if (str === "add") {
            this.addEmptyMachine();
            this.state.hasChange = true;
        } else if (str === "reset") {
            this.reset();
        }
    }

    reset() {
        this.state.machine = [];
        this.setState({});

        readTextFile();
        this.addMasterPoll();
        this.useData();
        //this.state.hasChange = false;
    }

    addDataToArray() {
        var name = "may " + data.miners.length;
        var tempArray = [];
        for (var index = 0; index < data.miners.length; index++) {
            var object = data.miners[index];
            var name = object.name;
            var host = object.host;
            var port = object.port;
            var pool = object.pool;
            tempArray.push({
                "name": name,
                "host": host,
                "port": port,
                "target_eth": 118,
                "target_dcr": 2700,
                "comments": "",
                "offline": false,
                "pool": pool
            });
        }

        miner.push({
            "name": name,
            "host": "",
            "port": "",
            "target_eth": 118,
            "target_dcr": 2700,
            "comments": "",
            "offline": false,
            "pool": ""
        });
        //data.miners = tempArray;
    }


    render() {
        return (
            //<PaneGroup>

            <Pane className={this.props.isActive ? 'no_scroll' : 'hidden no_scroll'}>
                <ConfigTitle onClick={(str) => {
                    this.handleEvent(str);
                    this.props.onClick(this.state.ip)
                }}>
                </ConfigTitle>
                <div id="main" ref="main">
                    {this.state.machine.map(function (input, index) {
                        return input;
                    })}
                </div>
            </Pane>

            //</PaneGroup>
        );

    }
}

export default Config
