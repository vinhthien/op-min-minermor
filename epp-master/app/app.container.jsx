import React from "react";
import ReactDom from "react-dom";
import { Window, Content, PaneGroup ,Pane } from "react-photonkit";

import Header from "./header.jsx"
import Footer from "./footer.jsx";
import Sidebar from "./sidebar.jsx"
import Config from "./config.container.jsx"
import Monitor from "./monitor.container.jsx"

require('../index.scss');
//require('../styles/app.scss');
var conFig;
class AppContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            mainViewIndex: 1,
            object: null,
            change: 1
        };

    }

    handleSelect(index) {
        var check = this.refs.config.state.hasChange;
        if (check) {
            if (index == 1) {
                index = 2;
                var sideBar = this.refs.sideBar
                //sideBar.props.handleSelect = 2;
                this.state.change = 2;
                this.setState({mainViewIndex: index})
                alert("hay save du lieu truoc khi thoat");
                return;
            }
        }
        this.state.change = index;

        this.setState({mainViewIndex: index})
        if (index == 1) { // monitor
            console.log(`I'm in Monitor view`);
        } else if (index == 2) { // config
            console.log(`I'm in Config view`);
        }

    }

    handleEvent(object) {
        if (object != null) {
            if (object.length > 0) {
                this.setState({object: object});
                //this.setState({object: null});
            }
        }
        object = null;
    }

    render() {

        return (
            <Window >
                <Header />
                <Content class="fix">
                    <PaneGroup>
                        <Sidebar ref="sideBar" change={this.state.change} handleSelect={this.handleSelect.bind(this)}/>
                        <Monitor ref="overview" isActive={this.state.mainViewIndex == 1} object={this.state.object}/>
                        <Config ref="config" isActive={this.state.mainViewIndex == 2} hasChange={false}
                                onClick={(object) => this.handleEvent(object)}/>

                    </PaneGroup>
                </Content>
            </Window>
        );
    }
}

export default AppContainer
