
function overview() {
    document.location.href = "overview.html";
}
function setting() {
    document.location.href = "setting.html";
}
var data = "";
function addExistMachine(object) {
    var ip = object.host;
    ip =  ip.split(/[.\-_]/);
    var ip1 = ip[0];
    var ip2 = ip[1];
    var ip3 = ip[2];
    var ip4 = ip[3];
    if(ip1 === 'undefined'){
        ip1 = "";
    }
    if(ip2 === undefined){
        ip2 = "";
    }
    if(ip3 === undefined){
        ip3 = "";
    }
    if(ip4 === undefined){
        ip4 = "";
    }
    var port = object.port;
    if(port === undefined){
        port = "";
    }
    var pool = object.pool;
    var name = "Máy " + (parseInt(data.miners.length));
    var elements = document.getElementsByClassName("main_content");
    var text = '<div class="main_content"><div class="float_left main_content_title">Cau hinh ' + name +'</div>' +
        ' <div class="main_content_element"> <div class="float_left full_width margin_top">Ten </div>' +
        ' <input class="main_content_input_large margin_top" id="name" value="' + object.name +'" placeholder="May 1">' +
        ' <div class="float_left full_width margin_top">Pools </div> ' +
        '<input class="main_content_input_large margin_top" id="pool" value="'+ pool +'" placeholder="Nhap dia chi Pool"><br>' +
        ' <div class="float_left full_width margin_top">Dia chi may (IP) </div> <br> <div>' +
        ' <input class="main_content_input_small margin_top" id="ip1" value="'+ ip1 +'" placeholder="127">' +
        ' <div class="speccial_character padding_top_twenty_five" >.</div>' +
        ' <input class="main_content_input_small margin_top" id="ip2" value="'+ ip2 +'" placeholder="0">' +
        ' <div class="speccial_character padding_top_center">.</div>' +
        ' <input class="main_content_input_small margin_top" id="ip3" value="'+ ip3 +'" placeholder="0">' +
        ' <div class="speccial_character padding_top_center">.</div> ' +
        '<input class="main_content_input_small margin_top" id="ip4" value="'+ ip4 +'" placeholder="0"> ' +
        '<div class="speccial_character padding_top_center">:</div> ' +
        '<input class="main_content_input_small margin_top" id="port" value="'+ port +'" placeholder="Port"> </div> <br> <br><br> </div></div>';
    var main = document.getElementById("main");
    var mainContent = document.createElement("div");
    mainContent.className = "main_content";
    mainContent.innerHTML = mainContent.innerHTML + text;
    main.innerHTML = main.innerHTML + mainContent.innerHTML;
}
function addEmptyMachine() {
    var name = 'máy ' + (parseInt(data.miners.length) + 1);
    var elements = document.getElementsByClassName("main_content");
    var text = '<div class="main_content"><div class="float_left main_content_title">Cau hinh ' + name +'</div> ' +
        '<div class="main_content_element"> <div class="float_left full_width margin_top">Ten </div>' +
        ' <input class="main_content_input_large margin_top" id="name"  value="' + name +'" placeholder="May 1">' +
        ' <div class="float_left full_width margin_top">Pools </div> ' +
        '<input class="main_content_input_large margin_top" id="pool" placeholder="Nhap dia chi Pool"><br>' +
        ' <div class="float_left full_width margin_top">Dia chi may (IP) </div> <br> <div> ' +
        '<input class="main_content_input_small margin_top" id="ip1" placeholder="127"> ' +
        '<div class="speccial_character padding_top_twenty_five" >.</div> ' +
        '<input class="main_content_input_small margin_top" id="ip2" placeholder="0"> ' +
        '<div class="speccial_character padding_top_center">.</div> ' +
        '<input class="main_content_input_small margin_top" id="ip3" placeholder="0"> ' +
        '<div class="speccial_character padding_top_center">.</div> ' +
        '<input class="main_content_input_small margin_top" id="ip4" placeholder="0"> ' +
        '<div class="speccial_character padding_top_center">:</div> ' +
        '<input class="main_content_input_small margin_top" id="port" placeholder="Port"> </div> <br> <br><br> </div></div>';
    var main = document.getElementById("main");
    var mainContent = document.createElement("div");
    mainContent.className = "main_content";
    mainContent.innerHTML = mainContent.innerHTML + text;
    main.innerHTML = main.innerHTML + mainContent.innerHTML;
}

function readTextFile() {
    var jsonfile = require('jsonfile')
    var file = 'config.json'
    //jsonfile.readFile(file, function (err, obj) {
    //    data = obj
    //})
    data = jsonfile.readFileSync(file)
    if (data != "") {
        var count = data.miners.length;
        if (count !== 'undefined' || count > 0) {
            for (var index = 0; index < count; index++) {
                addExistMachine(data.miners[index]);
            }
            checkForcePool(true);
        }

    }
}

function checkForcePool(enable){
    var mainContent = document.getElementsByClassName("main_content");
    document.getElementById("main_pool").value = data.force_pool4;

    if (mainContent.length > 1) {
        var tempArray = [];
        if (data.force_pool_enable === true) {
            document.getElementById("main_pool").disabled = false;
        }else{
            document.getElementById("main_pool").disabled = true;
        }
        for (var index = 1; index < mainContent.length; index++) {
            var currentMainContent = mainContent[index];
            if (data.force_pool_enable === true) {
                currentMainContent.querySelector('#pool').disabled = true;
            }else{
                currentMainContent.querySelector('#pool').disabled = false;
            }

        }
    }
    if(enable === true){
        if (data.force_pool_enable === false) {
            document.getElementById("onswitch").click();
            switchForcePoll();
            checkForcePool(false);
            //document.getElementById("main_pool").disabled = true;
        }
    }
}

function switchForcePoll() {
    if (data.force_pool_enable === true) {
        data.force_pool_enable = false;
    } else {
        data.force_pool_enable = true;
    }
    checkForcePool();
}
    function save() {
        var mainContent = document.getElementsByClassName("main_content");
        var mainPool = document.getElementById("main_pool");
        if (mainContent.length > 1) {
            var tempArray = [];
            for (var index = 1; index < mainContent.length; index++) {
                var currentMainContent = mainContent[index];
                var name = currentMainContent.querySelector('#name').value;
                var pool = currentMainContent.querySelector('#pool').value;
                var ip1 = currentMainContent.querySelector('#ip1').value;
                var ip2 = currentMainContent.querySelector('#ip2').value;
                var ip3 = currentMainContent.querySelector('#ip3').value;
                var ip4 = currentMainContent.querySelector('#ip4').value;
                var ip = ip1 + "." + ip2 + "." + ip3 + "." + ip4
                if (ip1 === "" && ip2 === "" && ip3 === "" && ip4 === "") {
                    ip = ""
                }
                data.force_pool = mainPool.value;
                var port = currentMainContent.querySelector('#port').value;
                tempArray.push({
                    "name": name,
                    "host": ip,
                    "port": port,
                    "target_eth": 118,
                    "target_dcr": 2700,
                    "comments": "",
                    "offline": false,
                    "pool": pool
                });
                data.miners = tempArray;

            }
            var jsonfile = require('jsonfile')
            var file = 'config.json'

            jsonfile.writeFile(file, data, function (err) {
                var asd = err
                var das = asd
                console.error(err)
            })
            document.getElementById("update_success").style.visibility = "visible";

        }

    }

    window.onload = function () {
        readTextFile();
    }
