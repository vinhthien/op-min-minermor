/**
 * Created by vinhthien on 5/4/17.
 */
function state() {
    document.getElementById("state_dropdown").classList.toggle("show");
}
function day() {
    document.getElementById("day_dropdown").classList.toggle("show");
}
function overview(){
    document.location.href = "overview.html";
}
function setting(){
    document.location.href = "setting.html";
}
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}